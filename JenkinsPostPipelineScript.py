import requests, base64, json, sys

def update_issue(issueid, build_number) :
    try :
        data = {
           "fields":{
               "customfield_15100":build_number,
            },
        }
        print(data)
        update_response = session_request.put(Jira_URL+'rest/api/2/issue/'+str(issueid), data = json.dumps(data))
        print(update_response)
        print(update_response.content)
        if update_response.status_code == 204 :
            return True
        else :
            return False
    except Exception as e :
        return False

def transition_issue(issueid) :
    try :
        data = {
            'transition':{
                'id' : '901'
            }
        }
        print(data)
        update_response = session_request.post(Jira_URL+'rest/api/2/issue/'+str(issueid)+'/transitions', data = json.dumps(data))
        print(update_response)
        print(update_response.content)
        if update_response.status_code == 204 :
            return True
        else :
            return False
    except Exception as e :
        return False


session_request = requests.Session()
session_request.auth = ('sakumar','rsys@213456')
session_request.headers.update({"Content-Type": "application/json"})
Jira_URL = 'http://in-alm01.radisys.com:8081/jira/'

issueid = sys.argv[1]
build_number = sys.argv[2]
if not issueid or not build_number :
    print("Need to provide issueid, build_number")
    exit(1)
update_status = update_issue(issueid, build_number)
if update_status :
    print("Updated the build number(",build_number,") in issue : ", issueid)
else :
    print("Failed to update the issue : ", issueid)


